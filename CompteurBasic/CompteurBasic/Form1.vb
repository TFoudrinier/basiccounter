﻿Imports ClassLibraryCompteurBasic
Public Class Form1
    Private Sub btn_incrementation_Click(sender As Object, e As EventArgs) Handles btn_incrementation.Click
        LabelCompteur.Text = Compteur.Incrementation(LabelCompteur.Text)
    End Sub

    Private Sub btn_decrementation_Click(sender As Object, e As EventArgs) Handles btn_decrementation.Click
        LabelCompteur.Text = Compteur.Decrementation(LabelCompteur.Text)
    End Sub

    Private Sub btn_reset_Click(sender As Object, e As EventArgs) Handles btn_reset.Click
        LabelCompteur.Text = Compteur.Reset()
    End Sub
End Class
