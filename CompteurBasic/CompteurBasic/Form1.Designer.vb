﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_decrementation = New System.Windows.Forms.Button()
        Me.btn_incrementation = New System.Windows.Forms.Button()
        Me.btn_reset = New System.Windows.Forms.Button()
        Me.LabelCompteur = New System.Windows.Forms.Label()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_decrementation
        '
        Me.btn_decrementation.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_decrementation.BackColor = System.Drawing.SystemColors.ControlDark
        Me.btn_decrementation.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_decrementation.Font = New System.Drawing.Font("Rubik", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_decrementation.Location = New System.Drawing.Point(105, 104)
        Me.btn_decrementation.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_decrementation.Name = "btn_decrementation"
        Me.btn_decrementation.Size = New System.Drawing.Size(95, 30)
        Me.btn_decrementation.TabIndex = 0
        Me.btn_decrementation.Text = "-"
        Me.btn_decrementation.UseVisualStyleBackColor = False
        '
        'btn_incrementation
        '
        Me.btn_incrementation.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_incrementation.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.btn_incrementation.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_incrementation.Font = New System.Drawing.Font("Rubik", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_incrementation.Location = New System.Drawing.Point(296, 104)
        Me.btn_incrementation.Name = "btn_incrementation"
        Me.btn_incrementation.Size = New System.Drawing.Size(95, 30)
        Me.btn_incrementation.TabIndex = 1
        Me.btn_incrementation.Text = "+"
        Me.btn_incrementation.UseVisualStyleBackColor = False
        '
        'btn_reset
        '
        Me.btn_reset.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_reset.BackColor = System.Drawing.SystemColors.ControlDark
        Me.btn_reset.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_reset.Font = New System.Drawing.Font("Rubik", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_reset.Location = New System.Drawing.Point(200, 153)
        Me.btn_reset.Name = "btn_reset"
        Me.btn_reset.Size = New System.Drawing.Size(95, 30)
        Me.btn_reset.TabIndex = 2
        Me.btn_reset.Text = "RESET"
        Me.btn_reset.UseVisualStyleBackColor = False
        '
        'LabelCompteur
        '
        Me.LabelCompteur.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LabelCompteur.Font = New System.Drawing.Font("Roboto Cn", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelCompteur.Location = New System.Drawing.Point(200, 104)
        Me.LabelCompteur.Name = "LabelCompteur"
        Me.LabelCompteur.Size = New System.Drawing.Size(95, 30)
        Me.LabelCompteur.TabIndex = 3
        Me.LabelCompteur.Text = "0"
        Me.LabelCompteur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelTotal
        '
        Me.LabelTotal.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LabelTotal.AutoSize = True
        Me.LabelTotal.Font = New System.Drawing.Font("Gadugi", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTotal.Location = New System.Drawing.Point(215, 60)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(65, 21)
        Me.LabelTotal.TabIndex = 4
        Me.LabelTotal.Text = "Total :"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(497, 268)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.LabelCompteur)
        Me.Controls.Add(Me.btn_reset)
        Me.Controls.Add(Me.btn_incrementation)
        Me.Controls.Add(Me.btn_decrementation)
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_decrementation As Button
    Friend WithEvents btn_incrementation As Button
    Friend WithEvents btn_reset As Button
    Friend WithEvents LabelCompteur As Label
    Friend WithEvents LabelTotal As Label
End Class
