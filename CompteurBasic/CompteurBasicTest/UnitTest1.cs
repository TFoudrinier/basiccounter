﻿using ClassLibraryCompteurBasic;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompteurBasicTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIncrementation()
        {
            Assert.AreEqual(1, Compteur.Incrementation(0));
            Assert.AreEqual(1000, Compteur.Incrementation(999));
            //Assert.AreEqual(1001, Compteur.Incrementation(1000));//doit echouer
        }
        [TestMethod]
        public void TestDecrementation()
        {
            //Assert.AreEqual(-1, Compteur.Decrementation(0));//doit echouer
            Assert.AreEqual(0, Compteur.Decrementation(0));
            Assert.AreEqual(0, Compteur.Decrementation(1));
        }
        [TestMethod]
        public void TestReset()
        {
            Assert.AreEqual(0, Compteur.Reset());
        }
    }
}
