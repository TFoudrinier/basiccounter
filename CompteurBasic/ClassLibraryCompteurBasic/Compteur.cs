﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryCompteurBasic
{
    public class Compteur
    {
        public int nb_compteur;

        public static int Incrementation(int nb_compteur)
        {
            if (nb_compteur < 1000)
            {
                nb_compteur = nb_compteur + 1;
            }
            
            return nb_compteur;
        }

        public static int Decrementation(int nb_compteur)
        {
            if (nb_compteur >= 1)
            {
                nb_compteur = nb_compteur - 1;
            }

            return nb_compteur;
        }

        public static int Reset()
        {
            int nb_compteur;
            nb_compteur = 0 ;
            return nb_compteur;
        }

    }
}
